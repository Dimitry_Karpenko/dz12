<?php

require_once 'classes/HourlyProject.php';
require_once 'classes/MilestoneProject.php';
require_once 'classes/Task.php';
require_once 'classes/ProjectFeedWriter.php';

$taskObjs = [];

$taskObjs[] = new Task('Проект', 'Описание проекта', '100');
$taskObjs[] = new HourlyProject('Хороший проект', 'Вот здесь подробное описание проекта', 10, 2, 100);
$taskObjs[] = new MilestoneProject('Проект', 'Описание проекта', '20', 2, 50);

$writer = new ProjectFeedWriter();

foreach ($taskObjs as $task)
{
  $writer->addProject($task);
}

echo $writer->getProjects();
