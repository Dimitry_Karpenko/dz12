<?php

require_once 'Project.php';

class HourlyProject extends Project
{

  protected $totalHouresToDo;
  protected $totalHouresDone;
  protected $oneHoureCost;

    public function __construct($title, $description, $totalHouresToDo, $totalHouresDone, $oneHoureCost)
  {
    parent::__construct($title, $description);
    $this->totalHouresToDo = $totalHouresToDo;
    $this->totalHouresDone = $totalHouresDone;
    $this->oneHoureCost = $oneHoureCost;
  }

  public function getPrice()
  {
    return $this->totalHouresToDo * $this->oneHoureCost;
  }

  public function getProjectProgress()
  {
    return $this->totalHouresDone / $this->totalHouresToDo;
  }

}