<?php


class ProjectFeedWriter
{

  protected $projects = [];

  public function addProject(Workable $project)
  {
    $this->projects[] = $project;
  }

  public function getProjects()
  {
    $str = '';

    foreach ($this->projects as $project)
    {
      $str .= '<h1>Название: '.$project->getTitle().'</h1>';
      $str .= '<p>Описание: '.$project->getDescription().'</p>';
      $str .= '<p>Стоимость: '.$project->getPrice().'</p>';
    }

    return $str;
  }



}