<?php

require_once 'Project.php';

class MilestoneProject extends Project
{
  protected $totalStages;
  protected $currentStage;
  protected $projectPrice;

  public function __construct($title, $description, $totalStages, $currentStage, $projectPrice)
  {
    parent::__construct($title, $description);
    $this->totalStages = $totalStages;
    $this->currentStage = $currentStage;
    $this->projectPrice = $projectPrice;
  }

  public function getPrice()
  {
    return $this->projectPrice;
  }

  public function getProjectProgress()
  {
    return $this->currentStage / $this->totalStages;
  }


}